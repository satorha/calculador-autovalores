#include "Determinante.h"

std::vector<std::vector<Numero_t> > matrizLiteral(const Matriz& m)
{
    auto dados = m.data();
    std::vector<std::vector<Numero_t> > retorno(dados.size());

    for(int i=0; i < retorno.size(); i++)
    {
        retorno[i] = unit(dados[i]);
    }

    return retorno;
}

std::vector<Numero_t> calcularDeterminante(std::vector<std::vector<Numero_t> > dados, int linhas, int colunas)
{
    if(linhas == 1 && colunas == 1)
    {
        return dados[0];
    }


    std::vector<Numero_t> total(1);

    Numero_t multiplicador = 1;
    for(int j =0; j < colunas; j++)
    {
        std::vector<Numero_t> tmp(1);
        tmp = soma(tmp, calcularDeterminante(removerLinhaEColuna(dados, colunas, 0, j), linhas-1, colunas-1));
        tmp = multiplica(tmp, dados[j]);
        tmp = multiplica(tmp, multiplicador);

        total = soma(total, tmp);

        multiplicador *= -1;
    }

    return total;
}


std::vector<std::vector<Numero_t> > removerLinhaEColuna(const std::vector<std::vector<Numero_t>>& in, int colunas, int linha_remover, int coluna_remover)
{
    std::vector<std::vector<Numero_t> > nova_matriz;

    for(size_t k=0; k < in.size(); k++)
    {
        int i = k / colunas;
        int j = k % colunas;

        if(i == linha_remover || j == coluna_remover)
            continue;

        nova_matriz.push_back(in[k]);
    }

    return nova_matriz;
}


void printa_matriz(const std::vector< std::vector<Numero_t> >& in, int colunas)
{
    for(size_t i=0; i < in.size(); i++)
    {
        if((i % colunas) == 0)
            std::cout << "\n";
        std::cout << mostra_polinomio(in[i]) << " | ";
    }
}
