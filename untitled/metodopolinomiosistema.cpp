#include "metodopolinomiosistema.h"

#include "Polinomios.h"
#include "Determinante.h"
#include "Sistema.h"
Resultados calcular_autovetores_polinomio(Matriz M)
{
    Resultados retorno;
    //converte a matriz pra uma matriz de polinomios
    auto mat_test = matrizLiteral(M);

    //monta a matriz (A - k*I)
    for(int i=0; i < M.colunas(); i++)
    {
        auto& referencia = elementoNaPos(mat_test, i, i, M.colunas());
        referencia = soma(referencia, {0, -1});
    }

    //calcula o polinomio característico
    retorno.polinomio_caracteristico = calcularDeterminante(mat_test, M.linhas(), M.colunas());


    //estima os autovalores pelo método da bisseção
    auto autovalores = estima_raizes(retorno.polinomio_caracteristico);

    Resultados resposta = calcular_autovetores(M, autovalores);

    retorno.autovetores = resposta.autovetores;

    return retorno;
}
