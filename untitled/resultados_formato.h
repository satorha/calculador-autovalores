#ifndef RESULTADOS_FORMATO_H
#define RESULTADOS_FORMATO_H

#include "Matriz.h"
#include "Polinomios.h"

struct Resultados
{
    std::vector<std::pair<Numero_t, Matriz> > autovetores;

    Polinomio polinomio_caracteristico;
};

#endif // RESULTADOS_FORMATO_H
