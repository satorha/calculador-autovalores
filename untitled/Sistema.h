#ifndef ELIMINACAOGAUSSIANA_H
#define ELIMINACAOGAUSSIANA_H

#include "Matriz.h"
#include "resultados_formato.h"

//acha uma solução para um sistema indeterminado
Matriz resolverIndeterminado(const Matriz& M);

//usa um sistema para calcular os autovetores de M usando os autovalores fornecidos
Resultados calcular_autovetores(const Matriz& M, std::vector<Numero_t> autovalores);


#endif // ELIMINACAOGAUSSIANA_H
