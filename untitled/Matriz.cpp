#include "Matriz.h"
#include <sstream>
#include <algorithm>
Matriz::Matriz(int linhas, int colunas)
{
    m_dados.resize(linhas*colunas);
    m_linhas = linhas;
    m_colunas = colunas;
}

Matriz::~Matriz()
{

}


void Matriz::setaLinha(int l, const Matriz& m)
{
    if(m.m_linhas != 1 || m.m_colunas != m_colunas)
        throw std::string("Matriz::setaLinha(): a matriz deve ter apenas uma linha e o mesmo número de colunas");

    for(size_t i=0; i < m.m_colunas; i++)
    {
        elemento(l, i) = m.elemento(0, i);
    }
}
void Matriz::setaColuna(int c, const Matriz& m)
{
    if(m.colunas() != 1 || m.linhas() != m_linhas)
        throw std::string("Matriz::setaLinha(): a matriz deve ter apenas uma linha e o mesmo número de colunas");

    for(int j=0; j < m.m_linhas; j++)
        elemento(j, c) = m.elemento(j, 0);
}

void Matriz::trocaLinhas(int l1, int l2)
{
    Matriz linha1 = pegaLinha(l1);
    Matriz linha2 = pegaLinha(l2);

    setaLinha(l2, linha1);
    setaLinha(l1, linha2);

}

#include "Determinante.h"
Numero_t Matriz::determinante() const
{
    auto mat_test = matrizLiteral(*this);
    auto det = calcularDeterminante(mat_test, linhas(), colunas());

    return det.front();
}

#include <iostream>
Matriz Matriz::formaEscada() const
{
    Matriz copia = *this;

    //assegura que o elemento (i,i) não é zero
    for(size_t i=0; i < copia.m_colunas; i++)
    {
        Matriz linha = copia.pegaLinha(i);

        //caso o elemento em (i, i), seja em zero, soma ele com alguma coisa para deixar de ser
        if(copia.elemento(i, i) == 0)
        {
            for(size_t w=i; w < copia.m_linhas; w++)
                if(copia.elemento(w, i) != 0)
                {
                    Matriz outra = copia.pegaLinha(w);
                    linha = linha + outra;
                    copia.setaLinha(i, linha);
                    break;
                }
        }


    }


    //sai eliminando todos os elementos acima e abaixo de (i, i)
     for(size_t i=0; i < copia.m_colunas; i++)
     {
         if(i >= copia.m_linhas)
            break;
         Matriz linha = copia.pegaLinha(i);

         for(size_t k=0; k < copia.m_linhas; k++)
         {
             if(k == i) continue;
             Numero_t atual = copia.elemento(k, i);
             if(atual == 0) continue;
             if(linha.elemento(0, i) == 0)
                continue;
             Matriz linha_pra_zerar = linha * (atual/linha.elemento(0, i));
             Matriz nova_linha = copia.pegaLinha(k) - linha_pra_zerar;
             copia.setaLinha(k, nova_linha);
         }
     }

     //normaliza as linhas, de forma que (i , i) seja 1
     for(size_t i=0; i < copia.m_linhas; i++)
     {
         Matriz linha = copia.pegaLinha(i);
         Numero_t atual = linha.elemento(0, i);
         if(atual == 0) continue;
         linha = linha * (1.0/atual);
         copia.setaLinha(i, linha);
     }


    return copia;
}

void Matriz::definirValor(std::vector< std::vector<Numero_t> > entrada)
{
    for(size_t i=0; i < entrada.size(); i++)
    {
        for(size_t j=0; j < entrada[i].size(); j++)
        {
            elemento(i, j) = entrada[i][j];
        }
    }
}

#include <iostream>

inline double modulo(double d)
{
    return (d < 0) ? -d : d;
}

Numero_t Matriz::normalizar()
{
    auto pos = posicaoMaiorValor();

    Numero_t maior = (elemento(pos.first, pos.second));

    if(maior == 0)
        return maior;

    for(Numero_t& n : m_dados)
        n /= maior;

    if(elemento(0,0) < 0)
        *this = (*this * -1);
    return maior;
}

Matriz Matriz::operator*(const Matriz& m) const
{
    if(m_colunas != m.m_linhas)
        throw std::string("Multiplicação não definida.");

    Matriz nova(m_linhas, m.m_colunas);

    for(int i=0; i < m_linhas; i++)
    {
        for(int j=0; j < m.m_colunas; j++)
        {
            Numero_t soma = 0;
            for(int atual = 0; atual < m_colunas; atual++)
                soma += elemento(i, atual) * m.elemento(atual, j);

            nova.elemento(i, j) = soma;
        }
    }

    return nova;
}

Matriz Matriz::operator-(const Matriz& m) const
{
    if(m_colunas != m.m_colunas || m_linhas != m.m_linhas)
        throw std::string("as matrizes devem ter as mesmas dimensões na subtração.");
    Matriz nova(m_linhas, m_colunas);

    for(int i=0; i < m_linhas; i++)
       for(int j=0; j < m_colunas; j++)
           nova.elemento(i, j) = (elemento(i, j) - m.elemento(i, j));

    return nova;
}

Matriz Matriz::operator+(const Matriz& m) const
{
    if(m_colunas != m.m_colunas || m_linhas != m.m_linhas)
        throw std::string("as matrizes devem ter as mesmas dimensões na adição.");
    Matriz nova(m_linhas, m_colunas);

    for(int i=0; i < m_linhas; i++)
       for(int j=0; j < m_colunas; j++)
           nova.elemento(i, j) = (elemento(i, j) + m.elemento(i, j));

    return nova;
}


Matriz Matriz::operator*(Numero_t v) const
{
    Matriz nova(m_linhas, m_colunas);

    for(int i=0; i < m_linhas; i++)
        for(int j=0; j < m_colunas; j++)
            nova.elemento(i, j) = (elemento(i, j) * v);

    return nova;
}

Matriz Matriz::pegaLinha(int l) const
{
    Matriz retorno(1, m_colunas);

    for(int i=0; i < m_colunas; i++)
    {
        retorno.elemento(0, i) = elemento(l, i);
    }
    return retorno;
}
Matriz Matriz::pegaColuna(int j) const
{
    Matriz retorno(m_linhas, 1);

    for(int i=0; i < m_linhas; i++)
        retorno.elemento(i, 0) = elemento(i, j);
    return retorno;
}

Matriz Matriz::matrizIndentidade(int ordem)
{
    Matriz nova(ordem, ordem);

    for(int i=0; i < ordem; i++)
        nova.elemento(i, i) = 1;

    return nova;
}

std::pair<int, int> Matriz::posicaoMaiorValor() const
{
    std::pair<int, int> pos_atual(0, 0);
    Numero_t maior_val = modulo(elemento(0, 0));

    for(int i=0; i < m_linhas; i++)
        for(int j=0; j < m_colunas; j++)
        {
            if(modulo(elemento(i, j)) > maior_val)
            {
                pos_atual = std::pair<int, int>(i, j);
                maior_val = modulo(elemento(i, j));
            }
        }
    return pos_atual;
}
#include <iomanip>
std::string Matriz::toString() const
{
    std::stringstream retorno;
    for(int i=0; i < m_linhas; i++)
    {
        retorno << "{";
        for(int j=0; j < m_colunas; j++)
        {
            retorno << std::setprecision (4) << std::fixed << elemento(i, j) << ((j != (m_colunas-1)) ? ", " : "");
        }
        retorno << "}\n";
    }
    return retorno.str();
}
#include <cstdio>

std::pair<Matriz, Numero_t> Matriz::acharMaiorAutovetor() const
{
    const Matriz& A = *this;
    if(!A.quadrada())
        throw std::string("Matriz deve ser quadrada para calcular os altovetores.");

    Matriz altovetor(A.m_linhas, 1);
    for(int i=0; i < A.m_linhas; i++)
        altovetor.elemento(i, 0) = 1;

    for(int i=0; i < 300; i++)
    {
        altovetor = A * altovetor;
            altovetor.normalizar();
    }

    //calcula o autovalor
    Matriz autovalor = (A * altovetor).transposta() * altovetor;
    Matriz autovalor_2 = altovetor.transposta()*altovetor;
    Numero_t av = autovalor.elemento(0,0)/autovalor_2.elemento(0,0);


    return std::pair<Matriz, Numero_t>(altovetor, av);
}

Matriz Matriz::transposta() const
{
    Matriz nova(m_colunas, m_linhas);

    for(int i=0; i < m_linhas; i++)
        for(int j=0; j < m_colunas; j++)
        {
            nova.elemento(j, i) = elemento(i, j);
        }

    return nova;
}
Matriz Matriz::subMatriz(int p_i, int p_j) const
{
    int linhas_nova = m_linhas - p_i;
    int colunas_nova = m_colunas - p_j;

    Matriz nova(linhas_nova, colunas_nova);

    for(int i=0; i < linhas_nova; i++)
        for(int j=0; j < colunas_nova; j++)
        {
            nova.elemento(i, j) = elemento(i+p_i, j+p_j);
        }

    return nova;
}

void umaSolucao(const Matriz& M)
{
    Matriz m_completa(M.linhas()+1, M.colunas()+1);

    for(int i=0; i < M.linhas(); i++)
        for(int j=0; j < M.colunas(); j++)
            m_completa.elemento(i, j) = M.elemento(i, j);

    m_completa = m_completa.formaEscada();
    m_completa.elemento(M.linhas(), 0) = 1;

    m_completa.elemento(M.linhas(), M.colunas()) = 1;
    m_completa = m_completa.formaEscada();

    std::cout << m_completa.toString() << std::endl;

    system("pause");

}

#include "Sistema.h"
std::vector<Matriz> Matriz::calcularAutovetores(std::vector<Numero_t> n) const
{
    for(Numero_t numero : n)
    {
        Matriz problema = (*this) - (matrizIndentidade(linhas()) * numero);


        Matriz resposta = resolverIndeterminado(problema);

    }

    return std::vector<Matriz>();

}

void Matriz::removerLinha(int linha)
{
    Matriz nova(linhas()-1, colunas());

    for(int i=0; i < linha; i++)
    {
        for(int j=0; j < colunas(); j++)
            nova.elemento(i, j) = elemento(i, j);
    }

    for(int i = linha; i < linhas()-1; i++)
    {
        for(int j=0; j < colunas(); j++)
            nova.elemento(i, j) = elemento(i+1, j);
    }

    *this = nova;
}

void Matriz::removerColuna(int coluna)
{
    Matriz nova(linhas(), colunas()-1);

    for(int i=0; i < linhas(); i++)
    {
        for(int j=0; j < coluna; j++)
            nova.elemento(i, j) = elemento(i, j);

        for(int j=coluna; j < colunas()-1; j++)
            nova.elemento(i, j) = elemento(i, j+1);
    }

    *this = nova;
}

std::vector< std::pair<Matriz, Numero_t> > Matriz::calcularAutovetores() const
{
    std::vector<std::pair<Matriz, Numero_t> > resposta;

    Matriz matriz_atual = *this;
    //acha o primeiro valor
    resposta.push_back(matriz_atual.acharMaiorAutovetor());

    for(int i=0; i < m_linhas-1; i++)
    {
        auto pos = resposta.back().first.posicaoMaiorValor();

        Matriz X = matriz_atual.pegaLinha(pos.first).transposta() * (1/(resposta.back().second*resposta.back().first.elemento(pos.first, pos.second)));
        Matriz B = (resposta.back().first*resposta.back().second) * X.transposta();
        matriz_atual = (matriz_atual - B);

        matriz_atual.removerColuna(0);
        matriz_atual.removerLinha(0);

        std::cout << "olha: " << matriz_atual.toString() << std::endl;

        resposta.push_back(matriz_atual.acharMaiorAutovetor());
    }

    std::vector<Numero_t> autovalores;
    for(auto& a : resposta)
        autovalores.push_back(a.second);

    for(auto& a : autovalores)
        std::cout << a << " ";
    //calcularAutovetores(autovalores);


    return resposta;
}

Numero_t Matriz::magnitude() const
{
    Numero_t mag = 0;

    for(Numero_t n : m_dados)
        mag += n*n;

    return sqrt(mag);
}
