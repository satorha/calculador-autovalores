#ifndef METODOQR_H
#define METODOQR_H

#include "resultados_formato.h"

Resultados calcular_autovetores_qr(Matriz M);
void decomposicao_householder(const Matriz& entrada, Matriz& Q, Matriz& R);

#endif // METODOQR_H
