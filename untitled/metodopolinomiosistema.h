#ifndef METODOPOLINOMIOSISTEMA_H
#define METODOPOLINOMIOSISTEMA_H
#include "definicoes.h"
#include "Matriz.h"
#include "Polinomios.h"
#include "resultados_formato.h"

/*
 * Calcula os autovalores de forma parecida com que calculamos a mão, isto é,
 * o polinômio característico é calculado e as raízes são os autovalores.
 *
 *
 * Com esses autovalores, é montado um sistema linear.
 *
 * Mais detalhes no relatório.
 *
 * */


Resultados calcular_autovetores_polinomio(Matriz M);
#endif // METODOPOLINOMIOSISTEMA_H
