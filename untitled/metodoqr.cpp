#include "metodoqr.h"
#include "Sistema.h"

void decomposicao_householder(const Matriz& entrada, Matriz& Q, Matriz& R)
{
    R = entrada;
    Q = Matriz::matrizIndentidade(entrada.linhas());
    for(int j=0; j < entrada.colunas(); j++)
    {
        Matriz X(entrada.linhas(), 1);
        Matriz V(entrada.linhas(), 1);

        //preenche X
        for(int i=j; i < X.linhas(); i++)
            X.elemento(i, 0) = R.elemento(i, j);


        //ajusta o valor de alpha
        Numero_t alpha = X.magnitude();
        alpha = (X.elemento(j, 0) < 0) ? alpha : -alpha;

        //preenche V
        for(int i=j; i < V.linhas(); i++)
            V.elemento(i, 0) = (j == i) ? (X.elemento(i, 0) + alpha) : (X.elemento(i, 0));


        if(V.magnitude() < epsilon) continue;

        V = V * (1/V.magnitude());

        Matriz P = Matriz::matrizIndentidade(V.linhas()) - ((V * V.transposta())*2);

        R = P * R;
        Q = Q * P;
    }
}

std::vector<Numero_t> calcular_autovalores_qr(Matriz M)
{
    Matriz res = M;
    Matriz Q(1,1);
    Matriz R(1,1);

    for(int i=0; i < 200; i++)
    {
        decomposicao_householder(res, Q, R);
        res = R*Q;
    }

    //os autovalores ficarão na diagonal
    std::vector<Numero_t> retorno;
    for(int i=0; i < res.colunas(); i++)
            retorno.push_back(res.elemento(i, i));

    return retorno;

}

Resultados calcular_autovetores_qr(Matriz M)
{
    return calcular_autovetores(M, calcular_autovalores_qr(M));
}
