#ifndef MATRIZ_H
#define MATRIZ_H

#include "definicoes.h"

#include <vector>
#include <string>

class Matriz
{
public:
    Matriz(int linhas, int colunas);
    ~Matriz();


    void definirValor(std::vector< std::vector<Numero_t> > entrada);

    //divide todos os números pelo maior dele
    //retorna este maior número
    Numero_t normalizar();
    Matriz transposta() const;

    Numero_t& elemento(int i, int j)
    {
        return m_dados[i*m_colunas + j];
    }
    const Numero_t& elemento(int i, int j) const
    {
        return m_dados[i*m_colunas + j];
    }
    Matriz operator*(const Matriz& m) const;
    Matriz operator-(const Matriz& m) const;
    Matriz operator+(const Matriz& m) const;
    Matriz operator*(Numero_t v) const;

    Numero_t determinante() const;
    std::string toString() const;

    bool quadrada() const
    {
        return (m_linhas == m_colunas);
    }

    std::pair<Matriz, Numero_t> acharMaiorAutovetor() const;
    std::vector< std::pair<Matriz, Numero_t> > calcularAutovetores() const;
    Matriz pegaLinha(int l) const;
    Matriz pegaColuna(int j) const;

    void setaLinha(int l, const Matriz& m);
    void setaColuna(int c, const Matriz& m);


    std::pair<int, int> posicaoMaiorValor() const;
    Numero_t maiorValor() const
    {
        return elemento(posicaoMaiorValor().first, posicaoMaiorValor().second);
    }

    Matriz subMatriz(int i, int j) const;

    static Matriz matrizIndentidade(int ordem);

    Matriz formaEscada() const;
    void trocaLinhas(int l1, int l2);
    void removerLinha(int l);
    void removerColuna(int c);

    int linhas() const
    {
        return m_linhas;
    }
    int colunas() const
    {
        return m_colunas;
    }

    const std::vector<Numero_t>& data() const
    {
        return m_dados;
    }

    //retorna sqrt( soma(Aij²) )
    Numero_t magnitude() const;
protected:
    std::vector<Matriz> calcularAutovetores(std::vector<Numero_t> autovalores) const;
private:
    int m_linhas;
    int m_colunas;
    std::vector<Numero_t> m_dados;


};

#endif // MATRIZ_H
