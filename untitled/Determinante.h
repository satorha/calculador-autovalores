#ifndef DETERMINANTE_H
#define DETERMINANTE_H

#include <vector>
#include <iostream>
#include "Polinomios.h"
#include "Matriz.h"

std::vector<std::vector<Numero_t> > matrizLiteral(const Matriz& m);

std::vector<Numero_t> calcularDeterminante(std::vector<std::vector<Numero_t> > dados, int linhas, int colunas);

std::vector<std::vector<Numero_t> > removerLinhaEColuna(const std::vector<std::vector<Numero_t>>& in, int colunas, int linha_remover, int coluna_remover);

void printa_matriz(const std::vector<std::vector<Numero_t> >& in, int colunas);

inline std::vector<Numero_t>& elementoNaPos(std::vector<std::vector<Numero_t> >& m, int i, int j, int colunas)
{
    return m[i*colunas + j];
}

#endif // DETERMINANTE_H
