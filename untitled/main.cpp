#include <cstdio>
#include <iostream>

#include "metodoqr.h"
#include "metodopolinomiosistema.h"

void mostra_autovetores(Resultados& r)
{
    for(auto& v : r.autovetores)
    {
        std::cout << "Autovalor: " << v.first << std::endl;
        std::cout << "Autovetor: " << v.second.toString() << std::endl;
    }
}

void faz_calculos(Matriz A)
{
    std::cout << "Metodo do polinomio caracteristico (bissecao)" << std::endl;
    Resultados bisec = calcular_autovetores_polinomio(A);
    std::cout << "Polinomio caracteristico: " << mostra_polinomio(bisec.polinomio_caracteristico) << std::endl;
    mostra_autovetores(bisec);

    std::cout << "Metodo QR:" << std::endl;
    Resultados qr = calcular_autovetores_qr(A);
    mostra_autovetores(qr);
}

int main()
{
    //Matriz A(3,3); A.definirValor({{2, 1, 1},{2, 3, 4},{-1, -1, 2}});
    //Matriz A(4,4); A.definirValor({{11, -6,  4, -2},{4,   1,  0,  0},{-9,  9, -6,  5},{-6,  6, -6,  7}});
    //Matriz A(5,5); A.definirValor( {{2, 1, 1, 8, 9},{2, 3, 4, 9,10},{-1, -1, 2,10,11}, {11, 12, 13, 14,12},{10, 80, 140, 20, 40}});
    //Matriz A(2,2); A.definirValor({ {2, 1}, {0, 2} });
    //Matriz A(2,2); A.definirValor({ {3, 7}, {9, 999} });

    int ordem=0;
    std::cout << "Digite a ordem da matriz: ";
    std::cin >> ordem;

    if(ordem < 0)
    {
        std::cout << "Ordem invalida.";
        return 0;
    }

    Matriz nova(ordem, ordem);

    std::cout << "Digite " << ordem*ordem << " elementos para preencher a matriz:" << std::endl;

    for(int i=0; i < ordem; i++)
    {
        for(int j=0; j < ordem; j++)
        {
            std::cin >> nova.elemento(i, j);
        }
    }

    //nova = A;
    faz_calculos(nova);

    return 0;


}
