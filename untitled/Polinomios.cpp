#include "Polinomios.h"

#include <sstream>
#include <iostream>

//remove os zeros a direita
void ajeita(std::vector<Numero_t>& pol)
{
    auto it = pol.end()-1;

    while(it != pol.begin() && (*it) == 0)
        it--;
    it++;

    pol.erase(it, pol.end());
}

Numero_t calcula(std::vector<Numero_t> polinomio, Numero_t x)
{
    Numero_t resultado=0;

    Numero_t x_atual = 1;

    for(Numero_t atual : polinomio)
    {
        resultado += atual * x_atual;
        x_atual *= x;
    }
    return resultado;
}


std::vector<Numero_t> deriva(std::vector<Numero_t> polinomio)
{
    std::vector<Numero_t> retorno = polinomio;
    retorno.erase(retorno.begin()); //apaga o i^0

    for(size_t i=0; i < retorno.size(); i++)
        retorno[i] *= (1+i);

    return retorno;
}

#include <algorithm>
std::string mostra_polinomio(std::vector<Numero_t> polinomio)
{
    std::stringstream str;


    for(size_t i=polinomio.size()-1; 1; i--)
    {
         if(polinomio[i] != 0)
         {
            if(i != polinomio.size()-1 || polinomio[i] < 0)
                str << ((polinomio[i] > 0) ?" + " : " - ");

            str << std::fabs(polinomio[i]);
            if(i > 1)
                str << "x^" << i;
            else if(i == 1)
                str<< "x";
         }

         if(i == 0)
             break;

    }


    return str.str();
}

inline bool positivo(Numero_t p)
{
    if(p >= 0)
        return true;
    return false;
}

Numero_t tolerancia = 0.000000000001;

const Numero_t range_pesquisa = 10000000000;
std::vector<Numero_t> estima_raizes(std::vector<Numero_t> polinomio)
{
    std::vector<Numero_t> respostas;
    if(polinomio.size() == 2)
    {
        return std::vector<Numero_t>{-polinomio[0]/polinomio[1]};
    }
    std::vector<Numero_t> locais = estima_raizes(deriva(polinomio));

    if(locais.size() == 0)
    {
        locais.push_back(-range_pesquisa);
        locais.push_back(range_pesquisa);
    }
    else
    {
        locais.push_back(locais.back() + range_pesquisa);
        locais.insert(locais.begin(), *locais.begin() - range_pesquisa);
    }

    //possivel raiz entre locais[i] e locais[i+1]
    for(size_t i=0; i < locais.size()-1; i++)
    {
        Numero_t a = locais[i], b=locais[i+1];

        if(calcula(polinomio, a) == 0)
        {
            respostas.push_back(a);
            continue;
        }

        if(positivo(calcula(polinomio, a)) == positivo(calcula(polinomio, b)))
            continue;



        for(int k=0; k < 5000; k++)
        {
            Numero_t meio = (a+b)/2;
            Numero_t atual = calcula(polinomio, meio);
            if(atual == 0 || (b - a) < tolerancia)
            {
                respostas.push_back(meio);
                break;
            }

            if(positivo(atual) == positivo(calcula(polinomio, a)))
                a = meio;
            else
                b = meio;
        }
    }
    return respostas;
}


std::vector<Numero_t> multiplica(const std::vector<Numero_t>& a, const std::vector<Numero_t>& b)
{
    std::vector<Numero_t> retorno(a.size() + b.size());
    for(int i=0; i < a.size(); i++)
    {
        for(int j=0; j < b.size(); j++)
        {
            retorno[i + j] += a[i]*b[j];
        }
    }

    ajeita(retorno);

    return retorno;
}

std::vector<Numero_t> multiplica(const std::vector<Numero_t>& a, Numero_t outro)
{
    std::vector<Numero_t> resposta = a;
    for(int i=0; i < a.size(); i++)
    {
        resposta[i] *= outro;
    }
    return resposta;
}

std::vector<Numero_t> soma(const std::vector<Numero_t>& a, const std::vector<Numero_t>& b)
{
    std::vector<Numero_t> resposta(std::max(a.size(), b.size()));

    for(int i=0; i < std::max(a.size(), b.size()); i++)
    {
        if(i < a.size()) resposta[i] += a[i];
        if(i < b.size()) resposta[i] += b[i];
    }
    return resposta;
}


std::vector<Numero_t> unit(Numero_t n)
{
    return {n};
}
