#ifndef POLINOMIOS_H
#define POLINOMIOS_H
#include <vector>
#include <string>
#include "definicoes.h"


typedef std::vector<Numero_t> Polinomio;

void ajeita(std::vector<Numero_t>& pol);
//estima as raizes do polinomio
std::vector<Numero_t> estima_raizes(std::vector<Numero_t> polinomio);

//printa o polinomio
std::string mostra_polinomio(std::vector<Numero_t> polinomio);

//deriva o polinomio
std::vector<Numero_t> deriva(std::vector<Numero_t> polinomio);

//multiplica dois polinomios
std::vector<Numero_t> multiplica(const std::vector<Numero_t>& a, const std::vector<Numero_t>& b);
std::vector<Numero_t> multiplica(const std::vector<Numero_t>& a, Numero_t outro);

std::vector<Numero_t> soma(const std::vector<Numero_t>& a, const std::vector<Numero_t>& b);


//calcula o valor do polinomio trocando a icognita pelo valor dado em x
Numero_t calcula(std::vector<Numero_t> polinomio, Numero_t x);

std::vector<Numero_t> unit(Numero_t n);



class Polinomios
{
public:
    Polinomios();
    ~Polinomios();

protected:

private:
};

#endif // POLINOMIOS_H
