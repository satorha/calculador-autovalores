TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Determinante.cpp \
    Matriz.cpp \
    Polinomios.cpp \
    metodopolinomiosistema.cpp \
    metodoqr.cpp \
    Sistema.cpp

HEADERS += \
    definicoes.h \
    Determinante.h \
    Matriz.h \
    Polinomios.h \
    metodopolinomiosistema.h \
    metodoqr.h \
    resultados_formato.h \
    Sistema.h

DISTFILES += \
    ../modelo.odt
