#include "Sistema.h"

#include <cmath>
#include <iostream>
#include <algorithm>

Matriz regraDeCramer(const Matriz& M, Matriz vetorResposta)
{
    Matriz resposta(1, M.colunas());

    if(vetorResposta.linhas() == 1)
        vetorResposta = vetorResposta.transposta();

    Numero_t determinante_principal = M.determinante();

    if(determinante_principal == 0)
        throw std::string("sistema indeterminado ou impossivel");

    for(int j=0; j < M.colunas(); j++)
    {
        Matriz copia = M;
        copia.setaColuna(j, vetorResposta);

        resposta.elemento(0, j) = copia.determinante()/determinante_principal;
    }

    return resposta;
}


Matriz resolverIndeterminado(const Matriz& M)
{
    for(int l=0; l < M.linhas(); l++)
    {
        Matriz vetorResposta(1, M.colunas());
        vetorResposta.elemento(0, l) = 1;
        for(int p=0; p < M.colunas(); p++)
        {
            Matriz problema = M;
            problema.elemento(l,p)++;


            try
            {
                Matriz resposta = regraDeCramer(problema, vetorResposta);

                if(std::fabs(resposta.elemento(0, p) - 1) < 0.020) //deve ter uma certa tolerancia
                    return resposta;
            }
            catch(std::string s)
            {
                //apenas continua o loop normalmente
            }


        }
    }

    return Matriz(0,0);
}


Resultados calcular_autovetores(const Matriz& M, std::vector<Numero_t> autovalores)
{
    //ordena os autovalores para sempre mostrar todos os autovetores na mesma ordem
    std::sort(autovalores.begin(), autovalores.end());

    Resultados retorno;

    for(auto a : autovalores)
    {
        //gera a matriz pra calcular o autovetor
        Matriz problema = M - (Matriz::matrizIndentidade(M.linhas()) * a);

        //acha uma solucao para o sistema indeterminado
        Matriz autovet = resolverIndeterminado(problema);

        if(autovet.linhas() == 0)
            //isso significa que ele não achou uma solução pro sistema. provavelmente o autovalor veio de um complexo
            continue;

        if(fabs(a) < epsilon)
            a = 0;
        retorno.autovetores.push_back(std::pair<Numero_t, Matriz>(a, autovet));
    }

    return retorno;
}

